variables:
  GIT_SUBMODULE_STRATEGY: normal
  MEDIALIBRARY_IMG: registry.videolan.org/medialibrary:20220128153337
  MEDIALIBRARY_WIN32_IMG: registry.videolan.org/medialibrary-win32:20220502075714
  MEDIALIBRARY_WIN64_IMG: registry.videolan.org/medialibrary-win64:20220502082738
  VLC_DEBIAN_UNSTABLE_IMG: registry.videolan.org/vlc-debian-unstable:20220127084320
  MEDIALIBRARY_ALPINE_IMG: registry.videolan.org/medialibrary-alpine:20210902074848
  MEDIALIBRARY_ARCH_IMG: registry.videolan.org/medialibrary-archlinux:20220124130611
  MEDIALIB_TEST_FOLDER: $CI_PROJECT_DIR/medialib_tests/

stages:
  - build
  - test
  - generate

default:
  tags:
    - docker
    - amd64
  interruptible: true

build:novlc:
  image: $MEDIALIBRARY_IMG
  stage: build
  rules:
    - if: $CI_MERGE_REQUEST_IID
  script:
    - meson -Dlibvlc=disabled --buildtype=release build
    - cd build && ninja

build:alpine:
  image: $MEDIALIBRARY_ALPINE_IMG
  stage: build
  rules:
    - if: $CI_MERGE_REQUEST_IID
  script:
    - meson build
    - cd build && ninja

build:arch:
  image: $MEDIALIBRARY_ARCH_IMG
  stage: build
  rules:
    - if: $CI_MERGE_REQUEST_IID
  script:
    - meson build
    - cd build && ninja

test:debian:
  image: $MEDIALIBRARY_IMG
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $MEDIALIB_MANUAL_JOB_NAME == null'
  stage: test
  script:
    - cd /tmp/ && git clone --single-branch --branch=display_stack_on_timeout --depth=1 https://github.com/chouquette/meson
    - export PATH=/tmp/meson:$PATH
    - cd $CI_PROJECT_DIR
    - meson.py -Db_coverage=true build
    - cd build
    - meson.py test --no-stdsplit
    - mkdir html/
    - >
      gcovr -r "$CI_PROJECT_DIR/"
      -e "$CI_PROJECT_DIR/libvlcpp"
      -e "$CI_PROJECT_DIR/test"
      -e "$CI_PROJECT_DIR/src/database/SqliteErrors.h"
      -e "$CI_PROJECT_DIR/src/database/SqliteErrors.cpp"
      -e "$CI_PROJECT_DIR/include/medialibrary/filesystem/Errors.h"
      -e "$CI_PROJECT_DIR/include/medialibrary/IMediaLibrary.h"
      -e "$CI_PROJECT_DIR/src/utils/xxhash/"
      --xml cobertura.xml --html=html/medialibrary.html --html-details
      -s
      -j 4
  artifacts:
    reports:
      coverage_report: 
        coverage_format: cobertura
        path: build/cobertura.xml
      junit: build/meson-logs/testlog.junit.xml
    name: "coverage-medialibrary-$CI_COMMIT_SHORT_SHA"
    paths:
      - build/html/
      - $MEDIALIB_TEST_FOLDER/**/test.db
      - $CI_PROJECT_DIR/build/meson-logs/testlog.txt
    when: always

test:win32:
  image: $MEDIALIBRARY_WIN32_IMG
  variables:
    MESON_TESTTHREADS: 8
  stage: test
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == "$CI_DEFAULT_BRANCH@videolan/medialibrary"'
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
  script:
    - >
      meson -Dpkg_config_path=/prefix/lib/pkgconfig
      --cross-file=/opt/crossfiles/i686-w64-mingw32.meson
      -Ddefault_library=static
      build
    - cd build && ninja
    - cp /prefix/dll/libvlc.dll .
    - cp /prefix/dll/libvlccore.dll .
    - ln -s /prefix/lib/vlc/plugins/ .
    - wineserver -p && wine wineboot
    - MEDIALIB_TEST_FOLDER=`winepath -w $MEDIALIB_TEST_FOLDER` meson test --no-stdsplit
  artifacts:
    when: always
    reports:
      junit: build/meson-logs/testlog.junit.xml
    paths: 
      - $MEDIALIB_TEST_FOLDER/**/test.db
      - $CI_PROJECT_DIR/build/meson-logs/testlog.txt
    expire_in: 1 week

test:win64:
  image: $MEDIALIBRARY_WIN64_IMG
  variables:
    MESON_TESTTHREADS: 8
  stage: test
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == "$CI_DEFAULT_BRANCH@videolan/medialibrary"'
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
  script:
    - >
      PKG_CONFIG_PATH=/prefix/lib/pkgconfig
      meson
      --cross-file=/opt/crossfiles/x86_64-w64-mingw32.meson
      build
    - cd build && ninja
    - wineserver -p && wine wineboot
    - cp /prefix/dll/libvlc.dll .
    - cp /prefix/dll/libvlccore.dll .
    - ln -s /prefix/lib/vlc/plugins/ .
    - MEDIALIB_TEST_FOLDER=`winepath -w $MEDIALIB_TEST_FOLDER` meson test --no-stdsplit
  artifacts:
    when: always
    reports:
      junit: build/meson-logs/testlog.junit.xml
    paths: 
      - $MEDIALIB_TEST_FOLDER/**/test.db
      - $CI_PROJECT_DIR/build/meson-logs/testlog.txt
    expire_in: 1 week

asan-ubsan:
  image: $VLC_DEBIAN_UNSTABLE_IMG
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $MEDIALIB_MANUAL_JOB_NAME == null'
  stage: test
  variables:
    LSAN_OPTIONS: 'detect_leaks=0'
  script:
    - git clone https://code.videolan.org/videolan/vlc.git --depth=1
    - cd vlc && ./bootstrap
    - ./configure LDFLAGS="-lasan -lubsan" --prefix=$(pwd)/prefix --disable-qt --with-sanitizer=address,undefined --disable-medialibrary --disable-nls --enable-debug
    - make install -j8
    - export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:$(pwd)/prefix/lib/pkgconfig"
    - cd /tmp/ && git clone --single-branch --branch=display_stack_on_timeout --depth=1 https://github.com/chouquette/meson
    - export PATH=/tmp/meson:$PATH
    - cd $CI_PROJECT_DIR
    - meson.py -Db_sanitize=address,undefined build
    - cd build && meson.py test --no-stdsplit
    - meson.py compile fast_discover_cancel fast_teardown
    - ./test/fast_teardown/fast_teardown $CI_PROJECT_DIR/test/samples/samples
    - ./test/fast_discover_cancel/fast_discover_cancel $CI_PROJECT_DIR/test/samples/samples
  artifacts:
    when: on_failure
    paths:
      - $MEDIALIB_TEST_FOLDER/**/test.db
      - $CI_PROJECT_DIR/build/meson-logs/testlog.txt
    expire_in: 1 week

.base-sanitizer:
  image: $MEDIALIBRARY_IMG
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == "$CI_DEFAULT_BRANCH@videolan/medialibrary"'
  stage: test
  script:
    - cd /tmp/ && git clone --single-branch --branch=display_stack_on_timeout --depth=1 https://github.com/chouquette/meson
    - export PATH=/tmp/meson:$PATH
    - cd $CI_PROJECT_DIR
    - >
      CXX=clang++
      meson.py
      -Dpkg_config_path=$PKG_CONFIG_PATH:$CI_PROJECT_DIR/vlc/prefix/lib/pkgconfig
      -Db_sanitize=$SANITIZERS
      -Db_lundef=false
      build
    - cd build && meson.py test --no-stdsplit
    - meson.py compile fast_discover_cancel fast_teardown
    - ./test/fast_teardown/fast_teardown $CI_PROJECT_DIR/test/samples/samples
    - ./test/fast_discover_cancel/fast_discover_cancel $CI_PROJECT_DIR/test/samples/samples
  artifacts:
    when: on_failure
    paths:
      - $MEDIALIB_TEST_FOLDER/**/test.db
      - $CI_PROJECT_DIR/build/meson-logs/testlog.txt
    expire_in: 1 week

test:tsan:
  extends: .base-sanitizer
  variables:
    TSAN_OPTIONS: 'suppressions=$CI_PROJECT_DIR/ci/tsan_suppress_file'
    SANITIZERS: thread
        
test:asan-ubsan:
  extends: .base-sanitizer
  variables:
    LSAN_OPTIONS: 'detect_leaks=0'
    SANITIZERS: address,undefined

gen-test-db:
  image: $MEDIALIBRARY_IMG
  stage: generate
  dependencies: []
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $MEDIALIB_MANUAL_JOB_NAME == "gen-test-db"'
  script:
    - meson --buildtype=release build
    - cd build && ninja
    - ../ci/generate-samples.sh -o $CI_PROJECT_DIR/dummysamples -n 10
    - test/discoverer/discoverer $CI_PROJECT_DIR/dummysamples -q
    - echo "BEGIN;" > $CI_PROJECT_DIR/test_db.sql
    - > 
      sqlite3 $MEDIALIB_TEST_FOLDER/medialib/discoverer_test/test.db '.schema --nosys' 
      | grep -vF '/*' >> $CI_PROJECT_DIR/test_db.sql
    - >
      sqlite3 $MEDIALIB_TEST_FOLDER/medialib/discoverer_test/test.db '.dump --data-only --nosys' 
      | grep -v '^INSERT INTO [[:alpha:]]*Fts'
      >> $CI_PROJECT_DIR/test_db.sql
    - echo "COMMIT;" >> $CI_PROJECT_DIR/test_db.sql
    - mv $MEDIALIB_TEST_FOLDER/medialib/discoverer_test/test.db $CI_PROJECT_DIR/
  artifacts:
    when: on_success
    expire_in: 1 day
    paths:
      - "$CI_PROJECT_DIR/test_db.sql"
      - "$CI_PROJECT_DIR/test.db"
